
if (!$args[0]) {
    echo "Input file isn't specified!"
    echo "Usage: e2img <input file name> <optional:output name>"
    exit
}
if (![System.IO.File]::Exists($args[0])) {
    echo "Specified input file doesn't exist!"
    exit
}
$inp = $args[0]
if (!$args[1]) {
    $outp = "$($args[0]).txt"
}
else {
    $outp = $args[1]
}
[void] [System.Reflection.Assembly]::LoadWithPartialName('System.drawing') 
if ([System.IO.File]::Exists($outp)) {
    Remove-Item $outp
}
$Dat = Get-Date
$BitMap = [System.Drawing.Bitmap]::FromFile($inp)
$Out = "db $($BitMap.Width)"
$TOut = ""
$Mul = 100 / $BitMap.Height
Foreach ($y in (1..($BitMap.Height - 1))) { 
    Foreach ($x in (1..($BitMap.Width - 1))) { 
        $Clr = $BitMap.GetPixel($x, $y)
        $TOut = "$($TOut),$($Clr.R),$($Clr.G),$($Clr.B)"
    }
    $Out = "$($Out)$($TOut)"
    $TOut = ""
    echo "$($y*$Mul)%"
}
echo "Done in $((New-TimeSpan -Start $Dat -End (Get-Date)).TotalSeconds*1000)ms!"
echo "Saving in $($outp)"
Add-Content $outp $Out
exit